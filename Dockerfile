FROM debian:10-slim

WORKDIR /usr/local/bin/

RUN apt-get update && \
	apt-get install -y curl gnupg git && \
	curl -qsL https://storage.googleapis.com/kubernetes-release/release/$(curl -qsL https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl && \
	curl -qsL "https://github.com/mozilla/sops/releases/download/v3.7.1/sops-v3.7.1.linux" -o /usr/local/bin/sops && \
	curl -qsL "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh" | bash && \
	curl -qsL "https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3" | bash && \
	chmod +x /usr/local/bin/kubectl /usr/local/bin/kustomize /usr/local/bin/sops

WORKDIR /
